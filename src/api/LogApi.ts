import axios from 'axios';

export function LogList() {
  return axios.post('/api/log/list');
}

export function LogUpdate(request: any) {
  return axios.post('/api/log/update', request);
}

export function LogSave(request: any) {
  return axios.post('/api/log/save', request);
}

export function LogPage(request: any, page: any) {
  return axios.post('/api/log/page', Object.assign(request, page));
}

export function LogById(id: any) {
  return axios.post(`/api/log/byId/${id}`);
}

export function LogDeleteById(id: any) {
  return axios.post(`/api/log/delete/${id}`);
}

export function LogDeleteByIds(ids: any) {
  return axios.post(`/api/log/delete_ids`, ids);
}
