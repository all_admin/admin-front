import axios from 'axios';

export function RoleList() {
  return axios.post('/api/role/list');
}

export function RoleUpdate(request: any) {
  return axios.post('/api/role/update', request);
}
export function RoleSave(request: any) {
  return axios.post('/api/role/save', request);
}

export function RolePage(request: any, page: any) {
  return axios.post('/api/role/page', Object.assign(request, page));
}

export function RoleById(id: any) {
  return axios.post(`/api/role/byId/${id}`);
}
export function RoleDeleteById(id: any) {
  return axios.post(`/api/role/delete/${id}`);
}
export function RoleDeleteByIds(ids: any) {
  return axios.post(`/api/role/delete_ids`, ids);
}


export function RoleBindMenu(request: any) {
  return axios.post(`/api/role/bind/menu`,Object.assign(request));
}