import axios from 'axios';

export function ResApiList() {
  return axios.post('/api/res/api/list');
}

export function ResApiUpdate(request: any) {
  return axios.post('/api/res/api/update', request);
}

export function ResApiSave(request: any) {
  return axios.post('/api/res/api/save', request);
}

export function ResApiPage(request: any, page: any) {
  return axios.post('/api/res/api/page', Object.assign(request, page));
}

export function ResApiById(id: any) {
  return axios.post(`/api/res/api/byId/${id}`);
}

export function ResApiDeleteById(id: any) {
  return axios.post(`/api/res/api/delete/${id}`);
}

export function ResApiDeleteByIds(ids: any) {
  return axios.post(`/api/res/api/delete_ids`, ids);
}

export function ResMenuBindApi(ids: any) {
  return axios.post(`/api/res/menu/bind_api`, ids);
}
