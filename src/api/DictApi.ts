import axios from 'axios';

export function DictList() {
  return axios.post('/api/dict/list');
}
export function DictTree() {
  return axios.post('/api/dict/tree');
}

export function DictUpdate(request: any) {
  return axios.post('/api/dict/update', request);
}
export function DictSave(request: any) {
  return axios.post('/api/dict/save', request);
}

export function DictPage(request: any, page: any) {
  return axios.post('/api/dict/page', Object.assign(request, page));
}

export function DictById(id: any) {
  return axios.post(`/api/dict/byId/${id}`);
}
export function DictDeleteById(id: any) {
  return axios.post(`/api/dict/delete/${id}`);
}
export function DictDeleteByIds(ids: any) {
  return axios.post(`/api/dict/delete_ids`, ids);
}
