import axios from 'axios';

export function DictItemList() {
  return axios.post('/api/dict_item/list');
}

export function DictItemUpdate(request: any) {
  return axios.post('/api/dict_item/update', request);
}
export function DictItemSave(request: any) {
  return axios.post('/api/dict_item/save', request);
}

export function DictItemPage(request: any, page: any) {
  return axios.post('/api/dict_item/page', Object.assign(request, page));
}

export function DictItemById(id: any) {
  return axios.post(`/api/dict_item/byId/${id}`);
}
export function DictItemDeleteById(id: any) {
  return axios.post(`/api/dict_item/delete/${id}`);
}
export function DictItemDeleteByIds(ids: any) {
  return axios.post(`/api/dict_item/delete_ids`, ids);
}
