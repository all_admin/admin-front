import axios from 'axios';

export function UserList() {
  return axios.post('/api/user/list');
}

export function UserUpdate(request: any) {
  return axios.post('/api/user/update', request);
}
export function UserSave(request: any) {
  return axios.post('/api/user/save', request);
}

export function UserPage(request: any, page: any) {
  return axios.post('/api/user/page', Object.assign(request, page));
}

export function UserById(id: any) {
  return axios.post(`/api/user/${id}`);
}
export function UserDeleteById(id: any) {
  return axios.post(`/api/user/delete/${id}`);
}
export function UserDeleteByIds(ids: any) {
  return axios.post(`/api/user/delete_ids`, ids);
}

export function UserBindRole(request: any) {
  return axios.post('/api//user/bind/role', request);
}

export function UserBindDept(request: any) {
  return axios.post('/api/user/bind/dept', request);
}
