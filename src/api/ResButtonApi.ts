import axios from 'axios';

export function ResButtonList() {
  return axios.post('/api/res/button/list');
}

export function ResButtonUpdate(request: any) {
  return axios.post('/api/res/button/update', request);
}

export function ResButtonSave(request: any) {
  return axios.post('/api/res/button/save', request);
}

export function ResButtonPage(request: any, page: any) {
  return axios.post('/api/res/button/page', Object.assign(request, page));
}

export function ResButtonById(id: any) {
  return axios.post(`/api/res/button/byId/${id}`);
}

export function ResButtonDeleteById(id: any) {
  return axios.post(`/api/res/button/delete/${id}`);
}

export function ResButtonDeleteByIds(ids: any) {
  return axios.post(`/api/res/button/delete_ids`, ids);
}
