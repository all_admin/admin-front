import axios from 'axios';

export function ResMenuList() {
  return axios.post('/api/res/menu/list');
}

export function ResMenuUpdate(request: any) {
  return axios.post('/api/res/menu/update', request);
}

export function ResMenuSave(request: any) {
  return axios.post('/api/res/menu/save', request);
}

export function ResMenuPage(request: any, page: any) {
  return axios.post('/api/res/menu/page', Object.assign(request, page));
}

export function ResMenuById(id: any) {
  return axios.post(`/api/res/menu/byId/${id}`);
}

export function ResMenuDeleteById(id: any) {
  return axios.post(`/api/res/menu/delete/${id}`);
}

export function ResMenuDeleteByIds(ids: any) {
  return axios.post(`/api/res/menu/delete_ids`, ids);
}
export function MenuTree() {
  return axios.post(`/api/res/menu/tree`);
}
