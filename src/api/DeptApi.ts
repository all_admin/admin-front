import axios from 'axios';

export function DeptList() {
  return axios.post('/api/dept/list');
}

export function DeptTree() {
  return axios.post('/api/dept/tree');
}

export function DeptUpdate(request: any) {
  return axios.post('/api/dept/update', request);
}

export function DeptSave(request: any) {
  return axios.post('/api/dept/save', request);
}

export function DeptPage(request: any, page: any) {
  return axios.post('/api/dept/page', Object.assign(request, page));
}

export function DeptById(id: any) {
  return axios.post(`/api/dept/byId/${id}`);
}

export function DeptDeleteById(id: any) {
  return axios.post(`/api/dept/delete/${id}`);
}

export function DeptDeleteByIds(ids: any) {
  return axios.post(`/api/dept/delete_ids`, ids);
}
