import { computed } from 'vue';
import { RouteRecordNormalized, RouteRecordRaw } from 'vue-router';
import usePermission from '@/hooks/permission';
import { useAppStore, useUserStore } from '@/store';
import appClientMenus from '@/router/app-menus';
import { cloneDeep } from 'lodash';

export default function useMenuTree() {
  const permission = usePermission();
  const appStore = useAppStore();
  const appRoute = computed(() => {
    if (appStore.menuFromServer) {
      return appStore.appAsyncMenus;
    }
    return appClientMenus;
  });
  const userStore = useUserStore();

  const menuTree = computed(() => {
    const copyRouter = cloneDeep(appRoute.value) as RouteRecordNormalized[];
    copyRouter.sort((a: RouteRecordNormalized, b: RouteRecordNormalized) => {
      return (a.meta.order || 0) - (b.meta.order || 0);
    });
    function travel(_routes: RouteRecordRaw[], layer: number) {
      if (!_routes) return null;

      const collector: any = _routes.map((element) => {
        // no access
        if (!permission.accessRouter(element)) {
          return null;
        }

        // leaf node
        if (element.meta?.hideChildrenInMenu || !element.children) {
          element.children = [];
          return element;
        }

        // route filter hideInMenu true
        element.children = element.children.filter(
          (x) => x.meta?.hideInMenu !== true
        );

        // Associated child node
        const subItem = travel(element.children, layer + 1);

        if (subItem.length) {
          element.children = subItem;
          return element;
        }
        // the else logic
        if (layer > 1) {
          element.children = subItem;
          return element;
        }

        if (element.meta?.hideInMenu === false) {
          return element;
        }

        return null;
      });
      return collector.filter(Boolean);
    }

    const travel1 = travel(copyRouter, 0);

    const menus: string[] = [];
    userStore.menus.forEach((item) => {
      menus.push(<string>item.path);
    });

    function recursivePrintRoutes(route: any, parentPath = '', p = null) {
      const fullPath = `${parentPath}/${route.path}`.replace(/\/+/g, '/'); // Ensure correct path concatenation

      console.log(`Path: ${fullPath}`);

      if (p) {
        if (!menus.includes(fullPath)) {
          route.bbb = true;
        }
      }

      if (route.children && route.children.length > 0) {
        // eslint-disable-next-line no-restricted-syntax
        for (const childRoute of route.children) {
          recursivePrintRoutes(childRoute, fullPath, route.children);
        }
      }
    }

    // eslint-disable-next-line no-restricted-syntax
    for (const a of travel1) {
      recursivePrintRoutes(a);
    }

    function removeItemsWithBbb(data: []) {
      return data.reduce((acc: any, item: any) => {
        if (!(item.bbb === true)) {
          const children = item.children
            ? removeItemsWithBbb(item.children)
            : [];
          acc.push({ ...item, children });
        }
        return acc;
      }, []);
    }

    const bac = removeItemsWithBbb(travel1);

    return bac;
  });

  return {
    menuTree,
  };
}
