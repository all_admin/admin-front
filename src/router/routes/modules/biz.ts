import { DEFAULT_LAYOUT } from '../base';
import { AppRouteRecordRaw } from '../types';

const DASHBOARD: AppRouteRecordRaw = {
  path: '/',
  name: 'biz',
  component: DEFAULT_LAYOUT,
  meta: {
    locale: '系统管理',
    requiresAuth: true,
    icon: 'icon-dashboard',
    order: 1,
  },
  children: [
    {
      path: 'user',
      name: 'user_manager',
      component: () => import('@/views/user/UserIndex.vue'),
      meta: {
        locale: '用户管理',
        requiresAuth: true,
        roles: ['*'],
      },
    },

    {
      path: 'dept',
      name: 'dept_manager',
      component: () => import('@/views/dept/DeptIndex.vue'),
      meta: {
        locale: '部门管理',
        requiresAuth: true,
        roles: ['*'],
      },
    },
    {
      path: 'role',
      name: 'role_manager',
      component: () => import('@/views/role/RoleIndex.vue'),
      meta: {
        locale: '角色管理',
        requiresAuth: true,
        roles: ['*'],
      },
    },
    {
      path: 'dict',
      name: 'dict_manager',
      component: () => import('@/views/dict/DictIndex.vue'),
      meta: {
        locale: '字典管理',
        requiresAuth: true,
        roles: ['*'],
      },
    },
    {
      path: 'dict_item',
      name: 'dict_item_manager',
      component: () => import('@/views/dict/DictItemIndex.vue'),
      meta: {
        locale: '字典项管理',
        requiresAuth: true,
        roles: ['*'],
      },
    },
    {
      path: 'res_menu',
      name: 'res_menu_manager',
      component: () => import('@/views/res/ResMenuIndex.vue'),
      meta: {
        locale: '菜单管理',
        requiresAuth: true,
        roles: ['*'],
      },
    },
    {
      path: 'res_button',
      name: 'res_button_manager',
      component: () => import('@/views/res/ResButtonIndex.vue'),
      meta: {
        locale: '按钮管理',
        requiresAuth: true,
        roles: ['*'],
      },
    },
    {
      path: 'res_api',
      name: 'res_api_manager',
      component: () => import('@/views/res/ResApiIndex.vue'),
      meta: {
        locale: '接口管理',
        requiresAuth: true,
        roles: ['*'],
      },
    },
    {
      path: 'log',
      name: 'log_manager',
      component: () => import('@/views/log/LogIndex.vue'),
      meta: {
        locale: '日志管理',
        requiresAuth: true,
        roles: ['*'],
      },
    },
  ],
};

export default DASHBOARD;
